

const fs = require('fs');
const csv = require('csvtojson');
const path = require('path');
const csvFilePathDeliveries = path.resolve(__dirname,'../data/deliveries.csv');

    csv()
.fromFile(csvFilePathDeliveries)
.then((deliveries)=>{
    const  bowlerbestEconomy = besteconomybowler(deliveries);
    fs.writeFile('./src/public/output/bowler-besteconomy-in-super-over.json', JSON.stringify(bowlerbestEconomy), error=> {
        if(error)
            console.log(error);
        else
            console.log("Data is Written to  output file Succesfully");
    });
});
     
function besteconomybowler(deliveries) {
    const superoverBowlers = deliveries.reduce((acc,present) => {
        if(present.is_super_over != 0) {
            if(acc[present.bowler]){
                acc[present.bowler].conceded_runs += parseInt(present.total_runs - present.legbye_runs - present.bye_runs - present.penalty_runs);
                if (present.wide_runs == 0 && present.noball_runs == 0) {
                    acc[present.bowler].total_balls += 1;
                }

                acc[present.bowler].economy = (acc[present.bowler].conceded_runs)/ ((acc[present.bowler].total_balls /6));

            }
            else{ 
                acc[present.bowler] = {};
                acc[present.bowler].conceded_runs = parseInt(present.total_runs - present.legbye_runs - present.bye_runs - present.penalty_runs);
                acc[present.bowler].total_balls = 0;
                if (present.wide_runs == 0 && present.noball_runs == 0)
                    acc[present.bowler].total_balls = 1;
            }
        }
        return acc;
    }, {});
       //console.log(besteconomybowler);
       const sortdata=Object.fromEntries(
        Object.entries(superoverBowlers)
       .sort(([,a],[,b]) => {
       if(a.economy>b.economy)
           return 1;
       else if(a.economy<b.economy)
           return -1;
       else
           return 0;
   }).slice(0, 1));
    return sortdata;
}








    


     
        
    


