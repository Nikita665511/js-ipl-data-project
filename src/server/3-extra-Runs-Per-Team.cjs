

const fs = require('fs');
const csv = require('csvtojson');
const path = require('path');
const csvFilePathMatches = path.resolve (__dirname,'../data/matches.csv');
const csvFilePathDeliveries = path.resolve(__dirname,'../data/deliveries.csv');


var matchIds;
csv()
.fromFile(csvFilePathMatches)
.then((matches)=>{
    matchIds = findMatchIds(matches);
});

csv()
.fromFile(csvFilePathDeliveries)
.then((deliveries)=>{
    const extraRuns = extraRunsPerTeam(deliveries);
    fs.writeFile('./src/public/output/extra-Runs-Per-Teams.json', JSON.stringify(extraRuns), error=> {
        if(error)
            console.log(error);
        else
            console.log("Data is Written to  output file Succesfully");
    });
});


function findMatchIds(matches) {
    return matches.filter(match => match.season == '2016').map(match=> match.id);
}

 function extraRunsPerTeam(deliveries) {
   let extraRuns = {};
    deliveries.filter(delivery => matchIds.includes(delivery.match_id)).map(delivery => {
       if (matchIds.includes(delivery.match_id)) {
            if (extraRuns.hasOwnProperty(delivery.bowling_team)) {
               extraRuns[delivery.bowling_team] = extraRuns[delivery.bowling_team] + parseInt(delivery.extra_runs);
           } else {
               extraRuns[delivery.bowling_team] = parseInt(delivery.extra_runs);
            }
       }
    });
    return extraRuns;
}

