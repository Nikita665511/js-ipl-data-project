const fs = require('fs');
const csv = require('csvtojson');
const path = require('path');
const csvFilePathMatches = path.resolve(__dirname,'../data/matches.csv');

csv()
.fromFile(csvFilePathMatches)
.then((matches)=>{
    const matchcount = matchesPerYear(matches);
    const data = JSON.stringify(matchcount);
    fs.writeFile("/home/nikita/Desktop/js-ipl-data-project/src/public/output/matches-per-year.json",data,error=> {
        if(error)
        console.log(error);

        else
        console.log("Data is Written to  output file Succesfully");
    } )
    
}
)

function matchesPerYear(matches) {
    const seasons = matches.map((match)=>match.season);
    const matchcount = seasons.reduce((matches, season)=>{
        if(matches.hasOwnProperty(season)){
            matches[season]+= 1;
        }
        else{
            matches[season] = 1;
        }
        return matches;

    }, {});
    return matchcount;
}


