const fs = require('fs');
const csv = require('csvtojson');
const path = require('path');
const csvFilePathMatches = path.resolve(__dirname,'../data/matches.csv');

csv()
.fromFile(csvFilePathMatches)
.then((matches)=>{
    const winningTeams = matcheswonperteamperyear(matches);
    fs.writeFile('./src/public/output/matches-won-per-team-per-year.json',JSON.stringify(winningTeams),error=> {
        if(error)
        console.log(error);
        else
        console.log("Data is Written to  output file Succesfully");
    } )
}
)
function matcheswonperteamperyear(matches){
    const winningTeams = matches.reduce((teams, match)=>{
    if(teams.hasOwnProperty(match.winner))
    {
      if(teams[match.winner].hasOwnProperty(match.season))
      {
      teams[match.winner][match.season] += 1;}
      else
      {
        teams[match.winner][match.season] = 1;
      }
    }
    else{
        teams[match.winner] = {};
        teams[match.winner][match.season] = 1;
    }
    return teams;
   },{});
   return winningTeams;
}
  

