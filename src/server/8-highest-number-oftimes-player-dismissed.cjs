


const fs = require('fs');
const csv = require('csvtojson');
const path = require('path');
const csvFilePathDeliveries = path.resolve(__dirname,'../data/deliveries.csv');

    csv()
.fromFile(csvFilePathDeliveries)
.then((deliveries)=>{
    const  highestnumberoftimesplayerdismissed = playerdismissed(deliveries);
    fs.writeFile('./src/public/output/highest-number-of-times-player-dismissed.json', JSON.stringify(highestnumberoftimesplayerdismissed), error=> {
        if(error)
            console.log(error);
        else
            console.log("Data is Written to  output file Succesfully");
    });
});
function playerdismissed(deliveries)
{
    const dismissedbyBowlers = deliveries.reduce((acc,present) => {
        if(present.player_dismissed && acc[present.player_dismissed]) {
            if(acc[present.player_dismissed].hasOwnProperty(present.bowler))
            {
                acc[present.player_dismissed][present.bowler] += 1;
            }
            else
            {
                acc[present.player_dismissed][present.bowler] = 1;
            }
        }
        else if(present.player_dismissed) {
            acc[present.player_dismissed] = {};
            acc[present.player_dismissed][present.bowler] = 1;
        }
        return acc;
     } 
    ,{});

    let number_of_times_dismissed = 0;
    let dismissed_by = [];
    let returnData = {};
    let dismissed_player;

    Object.keys(dismissedbyBowlers).map((batsman) => {
        Object.keys(dismissedbyBowlers[batsman]).map((bowler) => {
            if(dismissedbyBowlers[batsman][bowler] == number_of_times_dismissed && dismissed_player == batsman)
            {
                dismissed_by.push(bowler);
            } else if (dismissedbyBowlers[batsman][bowler] > number_of_times_dismissed) {
                returnData = {}
                dismissed_player = batsman;
                number_of_times_dismissed = dismissedbyBowlers[batsman][bowler];
                dismissed_by = [];
                dismissed_by.push(bowler);
                player_data = {}
                player_data['dismissed_by'] = dismissed_by;
                player_data['number_of_times_dismissed'] = number_of_times_dismissed;
                returnData[dismissed_player] = player_data;
            }
        });
    });

    return returnData;
}

