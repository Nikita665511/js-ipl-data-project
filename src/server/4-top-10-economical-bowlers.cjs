
const fs = require('fs');
const csv = require('csvtojson');
const path = require('path');
const csvFilePathMatches = path.resolve(__dirname,'../data/matches.csv');
const csvFilePathDeliveries = path.resolve (__dirname,'../data/deliveries.csv');


var  matchIds;
csv()
.fromFile(csvFilePathMatches)
.then((matches)=>{
csv()
.fromFile(csvFilePathDeliveries)
.then((deliveries)=>{
    const top10Ecomomincalcbowlers = top10Economicalbowlers(matches,deliveries);
    fs.writeFile('./src/public/output/top-10-economical-bowlers.json', JSON.stringify(top10Ecomomincalcbowlers), error=> {
        if(error)
            console.log(error);
        else
            console.log("Data is Written to  output file Succesfully");
    });
});
});

function top10Economicalbowlers(matches,deliveries)
{
    const matchIds = matches.filter((match)=>match.season=='2015').map((match)=>match.id);
    
    const runsConceded = deliveries.reduce((acc,delivery)=>{
        if(matchIds.includes(delivery.match_id))
        {
            if(acc.hasOwnProperty(delivery.bowler))
            {
                if(acc[delivery.bowler].overs.includes(delivery.over))
                {
                    acc[delivery.bowler].runs += Number(delivery.total_runs);
                }
                else
                {
                    acc[delivery.bowler].overs.push(delivery.over);
                }
            }
                
            else
            {
                acc[delivery.bowler] = {};
                acc[delivery.bowler].runs = Number(delivery.total_runs);
                acc[delivery.bowler].overs = [];
                acc[delivery.bowler].overs.push(delivery.over);
            }
        }
        return acc;
    },{});

    Object.keys(runsConceded).map(key => {
        runsConceded[key]["economy"] = runsConceded[key]["runs"]/runsConceded[key]["overs"].length;
    });

    const sortedBowlers = Object.fromEntries(
        Object.entries(runsConceded).sort((a,b) => a[1].economy-b[1].economy)
    );

    const top10Economic = Object.fromEntries(
        Object.entries(sortedBowlers).slice(0,10)
      );

    let top10EconomicBowlers = {};
    Object.keys(top10Economic).map(key => {
        top10EconomicBowlers[key] = top10Economic[key].economy;
    });
        
    return top10EconomicBowlers;
}
    



