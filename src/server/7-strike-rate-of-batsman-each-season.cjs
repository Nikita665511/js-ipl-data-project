

const fs = require('fs');
const csv = require('csvtojson');
const path = require('path');
const csvFilePathMatches = path.resolve (__dirname ,'../data/matches.csv');
const csvFilePathDeliveries = path.resolve (__dirname,'../data/deliveries.csv');

var matchIds;
csv()
.fromFile(csvFilePathMatches)
.then((matches)=>{
    csv()
.fromFile(csvFilePathDeliveries)
.then((deliveries)=>{
    const  strikerateofbastmaneachseason = strikerateofbatsman(matches,deliveries);
    fs.writeFile('./src/public/output/strike-rate-of-Batsman-each-season.json', JSON.stringify(strikerateofbastmaneachseason), error=> {
        if(error)
            console.log(error);
        else
            console.log("Data is Written to  output file Succesfully");
    });
});
});

function strikerateofbatsman(matches,deliveries) {
    const player = matches.reduce((acc, match) => { 
        acc[match.id] = match.season  ;
        return acc;
    } ,{});

     
    const dataofplayer = deliveries.reduce((acc,delivery) =>{ 
        let season = player[delivery.match_id];
        if(acc.hasOwnProperty(delivery.batsman))
        {
            if(acc[delivery.batsman].hasOwnProperty(season)){
                acc[delivery.batsman][season].totalRuns += parseInt(delivery.total_runs);
                acc[delivery.batsman][season].totalBallsFaced += 1;
            }
            else{
                acc[delivery.batsman][season] = {};
                acc[delivery.batsman][season].totalRuns = parseInt(delivery.total_runs);
                acc[delivery.batsman][season].totalBallsFaced = 1;
            }

        }
        else{
            acc[delivery.batsman] = {};
            acc[delivery.batsman][season] = {};
            acc[delivery.batsman][season].totalRuns = parseInt(delivery.total_runs)
            acc[delivery.batsman][season].totalBallsFaced =1;
        }
        return acc;
    },{});

    const anwser = {};

    Object.keys(dataofplayer).map(batsman=>{
        let strike ={};
        Object.keys(dataofplayer[batsman]).map(year=>{
         let result = (dataofplayer[batsman][year].totalRuns/dataofplayer[batsman][year].totalBallsFaced)*100;
         strike[year] = result.toFixed(2);
        } )
        anwser[batsman] = strike;

    } );
    return anwser;
}



















