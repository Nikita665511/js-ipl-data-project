
const fs = require('fs');
const csv = require('csvtojson');
const path = require('path');
const csvFilePathMatches = path.resolve(__dirname,'../data/matches.csv');


//var matchIds;
csv()
.fromFile(csvFilePathMatches)
.then((matches)=>{

    const playerofthematch  = highestplayeroftheMatch (matches);
    fs.writeFile('./src/public/output/highest-Player-oftheMatch.json', JSON.stringify(playerofthematch), error=> {
        if(error)
            console.log(error);
        else
            console.log("Data is Written to  output file Succesfully");
    });
});

function highestplayeroftheMatch (matches){
    const playersOfMatch = matches.reduce((player, match)=>{
        if(player.hasOwnProperty(match.season))
        {
          if(player[match.season].hasOwnProperty(match.player_of_match))
          {
          player[match.season][match.player_of_match] += 1;}
          else
          {
            player[match.season][match.player_of_match] = 1;
          }
        }
        else{
            player[match.season] = {};
            player[match.season][match.player_of_match] = 1;
        }
        return player;
       },{});

       const highestplayerofthematch = Object.entries(playersOfMatch).reduce((player, everyyear) => {
        let year = Object.entries(everyyear[1]).sort(([,a],[,b])=>[,b][1]-[,a][1]).slice(0,1);
        player[everyyear[0]] = year[0];
        return  player;
   
       },{});
   
       return highestplayerofthematch;
    }
    

    




